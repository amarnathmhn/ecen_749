
// C program to continuously read slv_reg0, slv_reg1, slv_re2 using their respective
// base addresses and offsets specified in xparameters.h
// slv_reg2 is not being used, but is still read.
#include <xparameters.h>
#include <ir_demod.h>


int main() {
   int slv_reg0;          // stores slv_reg0 as integer
	int slv_reg0_val = 0;  // stores only Least significant 12 bits
	int slv_reg1;          // stores slv_reg1 as integer
	int slv_reg1_val = 0;  // stores only Least significant 12 bits
	int slv_reg2;          // stores slv_reg1 as integer
	int slv_reg2_val = 0;  // stores only Least significant 12 bits
	int last_dec_val;      // stores the last decoded value
	int last_cnt_val = 0;  // stores the latest message count. 
	                       // Shall be used as a demarker between messages 
								  // in a forever loop
	
	// Print to console
	xil_printf("\n\r Initiating Sony TV Remote IR demodulator \n\r");
	while (1) {
	   // read slv_reg0 which contains the actual 12bit value of IR message
		slv_reg0 = IR_DEMOD_mReadReg(XPAR_IR_DEMOD_0_BASEADDR,IR_DEMOD_SLV_REG0_OFFSET);
		// read slv_reg1 which contains the number of messages received till now
		slv_reg1 = IR_DEMOD_mReadReg(XPAR_IR_DEMOD_0_BASEADDR,IR_DEMOD_SLV_REG1_OFFSET);
		// read slv_reg2 which might be used for debug. Not used at this moment
		slv_reg2 = IR_DEMOD_mReadReg(XPAR_IR_DEMOD_0_BASEADDR,IR_DEMOD_SLV_REG2_OFFSET);
		// select the least significant 12  bits just to be sure
		slv_reg0_val = ( ((1<<12)-1) & slv_reg0);
		// if last count value is same as current, it means we're still decoding the current message.
		// So don't print if that is the case. Otherwise print it. Also slv_reg must be non zero 
		if( last_cnt_val != slv_reg1 & slv_reg0 != 0 ){
			// decode the received signal in this block.
			// the values in the demodulator verilog module ir_demod are stored in reverse order.
			switch(slv_reg0_val)
			{
			   // for Volume Up. Actual 0x490
				case 0x92: xil_printf("Received Signal = Ox%x\t Message Count = %d\t Volume Up\n\r", slv_reg0_val,slv_reg1);
						break;
				// for Channel Up. Actual 0x090
				case 0x90:  xil_printf("Received Signal = Ox%x\t Message Count = %d\t Channel Up\n\r", slv_reg0_val,slv_reg1);
						break;
				// for Channel 1. Actual 0x010
				case 0x80:  xil_printf("Received Signal = Ox%x\t Message Count = %d\t Channel 1\n\r", slv_reg0_val,slv_reg1);
						break;
				// for Channel 2. Actual 0x810
				case 0x81: xil_printf("Received Signal = Ox%x\t Message Count = %d\t Channel 2\n\r", slv_reg0_val,slv_reg1);
						break;
				default: xil_printf("Received Signal = Ox%x\t Message Count = %d\t Other Signal\n\r", slv_reg0_val,slv_reg1);
						break;
			}			
			
			last_cnt_val = slv_reg1;	
		}
	}
	return 0;
}



