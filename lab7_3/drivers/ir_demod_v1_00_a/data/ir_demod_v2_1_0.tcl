##############################################################################
## Filename:          /homes/grad/amarnathmhn/ECEN_749/lab7_3/drivers/ir_demod_v1_00_a/data/ir_demod_v2_1_0.tcl
## Description:       Microprocess Driver Command (tcl)
## Date:              Sun Mar 27 13:46:40 2016 (by Create and Import Peripheral Wizard)
##############################################################################

#uses "xillib.tcl"

proc generate {drv_handle} {
  xdefine_include_file $drv_handle "xparameters.h" "ir_demod" "NUM_INSTANCES" "DEVICE_ID" "C_BASEADDR" "C_HIGHADDR" 
}
