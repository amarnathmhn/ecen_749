//----------------------------------------------------------------------------
// user_logic.vhd - module
//----------------------------------------------------------------------------
//
// ***************************************************************************
// ** Copyright (c) 1995-2008 Xilinx, Inc.  All rights reserved.            **
// **                                                                       **
// ** Xilinx, Inc.                                                          **
// ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
// ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
// ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
// ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
// ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
// ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
// ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
// ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
// ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
// ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
// ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
// ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
// ** FOR A PARTICULAR PURPOSE.                                             **
// **                                                                       **
// ***************************************************************************
//
//----------------------------------------------------------------------------
// Filename:          user_logic.vhd
// Version:           1.00.a
// Description:       User logic module.
// Date:              Fri Mar 11 12:00:15 2016 (by Create and Import Peripheral Wizard)
// Verilog Standard:  Verilog-2001
//----------------------------------------------------------------------------
// Naming Conventions:
//   active low signals:                    "*_n"
//   clock signals:                         "clk", "clk_div#", "clk_#x"
//   reset signals:                         "rst", "rst_n"
//   generics:                              "C_*"
//   user defined types:                    "*_TYPE"
//   state machine next state:              "*_ns"
//   state machine current state:           "*_cs"
//   combinatorial signals:                 "*_com"
//   pipelined or register delay signals:   "*_d#"
//   counter signals:                       "*cnt*"
//   clock enable signals:                  "*_ce"
//   internal version of output port:       "*_i"
//   device pins:                           "*_pin"
//   ports:                                 "- Names begin with Uppercase"
//   processes:                             "*_PROCESS"
//   component instantiations:              "<ENTITY_>I_<#|FUNC>"
//----------------------------------------------------------------------------
module user_logic
(
  // -- ADD USER PORTS BELOW THIS LINE ---------------
  IR_signal ,                      // IR Signal
  // --USER ports added here 
  // -- ADD USER PORTS ABOVE THIS LINE ---------------

  // -- DO NOT EDIT BELOW THIS LINE ------------------
  // -- Bus protocol ports, do not add to or delete 
  Bus2IP_Clk,                     // Bus to IP clock
  Bus2IP_Reset,                   // Bus to IP reset
  Bus2IP_Data,                    // Bus to IP data bus
  Bus2IP_BE,                      // Bus to IP byte enables
  Bus2IP_RdCE,                    // Bus to IP read chip enable
  Bus2IP_WrCE,                    // Bus to IP write chip enable
  IP2Bus_Data,                    // IP to Bus data bus
  IP2Bus_RdAck,                   // IP to Bus read transfer acknowledgement
  IP2Bus_WrAck,                   // IP to Bus write transfer acknowledgement
  IP2Bus_Error                    // IP to Bus error response
  // -- DO NOT EDIT ABOVE THIS LINE ------------------
); // user_logic

// -- ADD USER PARAMETERS BELOW THIS LINE ------------
// --USER parameters added here 
// -- ADD USER PARAMETERS ABOVE THIS LINE ------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol parameters, do not add to or delete
parameter C_SLV_DWIDTH                   = 32;
parameter C_NUM_REG                      = 3;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

// -- ADD USER PORTS BELOW THIS LINE -----------------
// --USER ports added here 
input IR_signal;
// -- ADD USER PORTS ABOVE THIS LINE -----------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol ports, do not add to or delete
input                                     Bus2IP_Clk;
input                                     Bus2IP_Reset;
input      [0 : C_SLV_DWIDTH-1]           Bus2IP_Data;
input      [0 : C_SLV_DWIDTH/8-1]         Bus2IP_BE;
input      [0 : C_NUM_REG-1]              Bus2IP_RdCE;
input      [0 : C_NUM_REG-1]              Bus2IP_WrCE;
output     [0 : C_SLV_DWIDTH-1]           IP2Bus_Data;
output                                    IP2Bus_RdAck;
output                                    IP2Bus_WrAck;
output                                    IP2Bus_Error;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

//----------------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------------

  // --USER nets declarations added here, as needed for user logic

  // Nets for user logic slave model s/w accessible register example
  reg        [0 : C_SLV_DWIDTH-1]           slv_reg0;
  reg        [0 : C_SLV_DWIDTH-1]           slv_reg1;
  reg        [0 : C_SLV_DWIDTH-1]           slv_reg2;
  wire       [0 : 2]                        slv_reg_write_sel;
  wire       [0 : 2]                        slv_reg_read_sel;
  reg        [0 : C_SLV_DWIDTH-1]           slv_ip2bus_data;
  wire                                      slv_read_ack;
  wire                                      slv_write_ack;

  // --USER logic implementation added here
  // Start time   corresponds to 180,000 cycles of 75MHz Clock
  // Pulse 0 time corresponds to 45,000 cycles  of 75MHz
  // Pulse 1 time corresponds to 90,000 cycles  of 75 MHz clock 
 
  reg [32:0]	pulse_duration = 32'd0;    
  reg [11:0] 	output_signal  = 12'd0; 
  reg [3:0]	current_bit_index = 1'b0;
  reg		Bit_Result = 0;
  reg		processing = 0;
  reg 		count_start = 0;
  reg           start_detected = 0;
  reg           write = 0;  

   // to detect rising edge for IR_signal
   wire rise_IR_signal;
   reg  IR_signal_cur;  // current value of IR_signal
   reg  IR_signal_d1;   // IR signal with delay of one cycle

   // to detect falling edge for IR_signal
   wire fall_IR_signal;

   // logic for rising edge of IR signal
   always @(posedge Bus2IP_Clk) begin
      
      if( !Bus2IP_Reset) begin 
   
       IR_signal_cur <= IR_signal;      // store current value
       IR_signal_d1  <= IR_signal_cur;  // store previous value

      end
   end
   
   // detect edge
   assign rise_IR_signal  = IR_signal_cur & !IR_signal_d1;
   assign fall_IR_signal  = !IR_signal_cur & IR_signal_d1;

   // flags for edge risen and edge fallen
   reg  flag_rise;
   reg  flag_fall;
   
   always @(posedge rise_IR_signal, posedge fall_IR_signal) begin
   
         if(rise_IR_signal == 1) begin
            flag_rise <= 1;
	    flag_fall <= 0;
         end
         if(fall_IR_signal == 1) begin
            flag_fall <= 1;
            flag_rise <= 0;
         end
   end
   

  localparam BIT_ARRAY_LEN = 12;
  localparam START_WRITING = 1;
  localparam STOP_WRITING = 0;
  localparam START_PROCESSING = 1;
  localparam STOP_PROCESSING = 1;
  localparam START_INDX = 0;
  localparam START_COUNTING = 1;
  localparam STOP_COUNTING = 0;
  localparam INITIAL_CNT = 0;
  localparam LOW = 0;
  localparam HIGH = 1;
  localparam ZERO_DUR_MIN_LEN = 37500; //.5 ms worth of cycles
  localparam ZERO_DUR_MAX_LEN = 52500;   //.7 ms worth of cycles
  localparam ONE_DUR_MIN_LEN = 82500;    //1.1 ms worth of cycles
  localparam ONE_DUR_MAX_LEN = 97500;    //1.3 ms worth of cycles
  localparam START_DUR_MIN_LEN =160000;  //near start worth of cycles
  localparam TOTAL_BIT_COUNT =12;
  
  localparam IDLE=2'b00,  DECODE_BITS=2'b10;  
  
  reg [1:0]     state = IDLE;

  // ------------------------------------------------------
  // Example code to read/write user logic slave model s/w accessible registers
  // 
  // Note:
  // The example code presented here is to show you one way of reading/writing
  // software accessible registers implemented in the user logic slave model.
  // Each bit of the Bus2IP_WrCE/Bus2IP_RdCE signals is configured to correspond
  // to one software accessible register by the top level template. For example,
  // if you have four 32 bit software accessible registers in the user logic,
  // you are basically operating on the following memory mapped registers:
  // 
  //    Bus2IP_WrCE/Bus2IP_RdCE   Memory Mapped Register
  //                     "1000"   C_BASEADDR + 0x0
  //                     "0100"   C_BASEADDR + 0x4
  //                     "0010"   C_BASEADDR + 0x8
  //                     "0001"   C_BASEADDR + 0xC
  // 
  // ------------------------------------------------------

  assign
    slv_reg_write_sel = Bus2IP_WrCE[0:2],
    slv_reg_read_sel  = Bus2IP_RdCE[0:2],
    slv_write_ack     = Bus2IP_WrCE[0] || Bus2IP_WrCE[1] || Bus2IP_WrCE[2],
    slv_read_ack      = Bus2IP_RdCE[0] || Bus2IP_RdCE[1] || Bus2IP_RdCE[2];

  // implement slave model register(s)
  always @( posedge Bus2IP_Clk)
    begin: SLAVE_REG_WRITE_PROC

      if ( Bus2IP_Reset == 1 )
        begin
          slv_reg0 <= 0;
          slv_reg1 <= 0;
          slv_reg2 <= 0;
          pulse_duration <= 0; 
	  output_signal <= 0; 
  	  current_bit_index <= 0;
          Bit_Result <= 0;
  	  processing <= 0;
  	  count_start <= 0;
	  state <= IDLE;
        end
      else begin
            case(state) 
	     IDLE: begin
                if(flag_fall == 1) begin
	           
                    state <= DECODE_BITS;
	            
                end
                else state <= IDLE;
	     end
             
             DECODE_BITS: begin
                if(start_detected == 1) start_detected <= 0;
                
                if(current_bit_index == TOTAL_BIT_COUNT) begin
                        slv_reg0   <= output_signal;
	                slv_reg1   <= slv_reg1 + 1;
                        slv_reg2   <= 1;
                        current_bit_index <= 0;
	                
	        end
                else slv_reg2 <= 0;
 
                if(flag_rise == 1) begin
                
                    //check if pulse duration is for 0
                    if(pulse_duration >= ZERO_DUR_MIN_LEN && pulse_duration <= ZERO_DUR_MAX_LEN) begin
                        Bit_Result        <= 0;
	                current_bit_index <= current_bit_index + 1;
	                output_signal[current_bit_index] <= 0;
                        pulse_duration    <= 0;
                        state <= DECODE_BITS;
                    end
	            else if(pulse_duration >= ONE_DUR_MIN_LEN && pulse_duration <= ONE_DUR_MAX_LEN) begin
                        Bit_Result <= 1;
	                current_bit_index <= current_bit_index + 1;
	                output_signal[current_bit_index] <= 1;
                        pulse_duration    <= 0;
                        state <= DECODE_BITS;
                    end
                    else if( pulse_duration >= START_DUR_MIN_LEN) begin
                    
			start_detected <= 1;
                        state <= DECODE_BITS;
                        pulse_duration <= 0;
 		        current_bit_index <= 0;
		        Bit_Result <= 0;
	                output_signal <= 0;
                    end
                                      
                end
                
                else if( flag_fall == 1) begin
                    pulse_duration <= pulse_duration + 1;
                    state     <= DECODE_BITS; 
                end
                
             end
	    endcase
        end
     
    end // SLAVE_REG_WRITE_PROC


  // implement slave model register read mux
  always @( slv_reg_read_sel or slv_reg0 or slv_reg1 or slv_reg2 )
    begin: SLAVE_REG_READ_PROC

      case ( slv_reg_read_sel )
        3'b100 : slv_ip2bus_data <= slv_reg0;
        3'b010 : slv_ip2bus_data <= slv_reg1;
        3'b001 : slv_ip2bus_data <= slv_reg2;
        default : slv_ip2bus_data <= 0;
      endcase

    end // SLAVE_REG_READ_PROC

  // ------------------------------------------------------------
  // Example code to drive IP to Bus signals
  // ------------------------------------------------------------

  assign IP2Bus_Data    = slv_ip2bus_data;
  assign IP2Bus_WrAck   = slv_write_ack;
  assign IP2Bus_RdAck   = slv_read_ack;
  assign IP2Bus_Error   = 0;

endmodule
