`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:09:56 01/30/2016 
// Design Name: 
// Module Name:    switchTester 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module switchTester(
    LEDS,
	 SWITCHES
    );
	 
	 output [3:0] LEDS;
	 input [3:0] SWITCHES;
	 
	 assign LEDS = SWITCHES;


endmodule
