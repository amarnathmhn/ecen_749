#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>

#define DEVICE_FILE "/dev/multiplier"

int main() {

	unsigned int result;
	int fd;
	int i,j;
	int read_i, read_j;

	char input = 0;
	int bytes_write = 0;
	int *opBuff = (int*)malloc(3*sizeof(int));
	int *inBuff = (int*)malloc(2*sizeof(int));
	char *inCharBuff = (char*) inBuff;
	// Store the two operands to be multiplied in inBuff



	// open device file for reading and writing
	// using 'open' function operation to open it
	
	fd = open(DEVICE_FILE,O_RDWR);

        // handle error in opening file
        if(fd == -1){
		printf("Failed to open device file!\n");
		return -1;
	}

	while(input != 'q'){// continue unless user entered 'q'

	   for(i = 0; i <= 16; i++){
	   
		for(j = 0; j <= 16; j++){
	          inBuff[0] = i;
		  inBuff[1] = j;
		
		  // Write values to registers using char device
		  // use write to write i and j to peripheral
		  write(fd,inBuff,2*sizeof(int)); // write i and j operands
		  // read i,j, and result using char device
		  // use read to read from peripheral
		  // print unsigned ints to screen
                  
		  read(fd,opBuff,3*sizeof(int));
		  read_i = opBuff[0];
		  read_j = opBuff[1];
		  result = opBuff[2];
		  
		  printf("%u*%u = %u",read_i,read_j,result);

		  // Validate result
		  if(result == (i*j))
		     printf(" Result Correct! \n");
		  else
		     printf(" Result Incorrect!\n");

		  // read from terminal
		  input = getchar();
		}
	   }
	

	}


	close(fd);
	free(opBuff);
	free(inBuff);

	return 0;
}
