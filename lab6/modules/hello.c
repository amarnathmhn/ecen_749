#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
// define a static initializer function printing a basic Hello World string
static int __init my_init(void)
{
  printk(KERN_INFO "Hello World !\n");
  return 0;
}

// Function to invoke upon exit. Prints a simple Good Bye message

static void __exit my_exit(void)
{
   printk(KERN_ALERT "Good bye world!\n");
}

// Enter the module license
MODULE_LICENSE("GPL");
// Specify the authors
MODULE_AUTHOR("ECEN449 Amarnath and Gabriel");
// Add description
MODULE_DESCRIPTION("Simple Hello World Module");
// specify the initializer function
module_init(my_init);
// specify the exit function
module_exit(my_exit);
