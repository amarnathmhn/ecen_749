#include <linux/module.h>  // Needed by all Modules
#include <linux/kernel.h>  // Needed for KERN_* and printk
#include <linux/init.h>    // Needed for __init and __exit macros
#include <asm/io.h>        // Needed for IO reads and writes
#include <linux/fs.h>	
#include "xparameters.h"   // Needed for physical address of multiplier
#include "multiplier.h"
#include <linux/sched.h>
#include <asm/uaccess.h> 

// from xparameters.h
#define PHY_ADDR XPAR_MULTIPLY_0_BASEADDR //physical address of multiplier
// size of physical address range for multiply
#define MEMSIZE XPAR_MULTIPLY_0_HIGHADDR - XPAR_MULTIPLY_0_BASEADDR + 1


void* virt_addr;    // virtual address pointing to multiplier
char writeChar = 0; // To temporarily store the character taken from user space via get_user


// Assign function operation function pointers. Similar to chap 3 in book
static struct file_operations fops = {
  .read    =  device_read,
  .write   =  device_write,
  .open    =  device_open,
  .release =  device_release
};

// This function is run upon module load. This is where you setup data
// structures and reserve resources used by module.

static int __init my_init(void){

	// Linux Kernel's version of printf
	printk(KERN_INFO "Mapping virtual address ... \n");

	// map virtual address to multiplier physical address
	// use ioremap
	virt_addr = ioremap(PHY_ADDR,MEMSIZE);


	// register the multiply device
        
       /* This function call registers a device and returns a major number
        associated with it.*/
	Major = register_chrdev(0, DEVICE_NAME, &fops);

        /* Negative values indicate a problem . Handle this*/
        if (Major < 0) {		
           /* Make sure you release any other resources you've already
           grabbed if you get here so you don't leave the kernel in a
           broken state. */
          printk(KERN_ALERT "Registering char device failed with %d\n", Major);
          return Major;
        }

         printk(KERN_INFO "Registered a device with dynamic Major number of %d\n", Major);	
	
	// write 7 to register 0
	
//	printk(KERN_INFO "Writing 7 to register 0 \n");

//	iowrite32(7,virt_addr + 0);

	// Write 2 to register 1
//	printk(KERN_INFO "Writing a 2 to register 1\n");
	// use iowrite32
//	iowrite32(2,virt_addr + 4);
//	printk("Read %d from register 0\n", ioread32(virt_addr + 0));
//	printk("Read %d from register 1\n", ioread32(virt_addr + 4));
//	printk("Read %d from register 2\n", ioread32(virt_addr + 8));

	// A non zero return means init_module failed; module can't be loaded
	return 0;
}

// This function is run just prior to modules removal from system. You should
// release all resources used by your module here

static void __exit my_exit(void){



	//unregister before memory unmapping
	unregister_chrdev(Major, DEVICE_NAME);
	//unmap memory
	printk(KERN_ALERT "unmapping virtual address space...\n");
	iounmap((void*)virt_addr);

	
}

static int device_open(struct inode *inode, struct file *file){

	
  
  /* In these case we are only allowing one process to hold the device
     file open at a time. */
    
	printk("Opened device!!\n");  
  //}
  return 0;

}


static int device_release(struct inode *inode, struct file *file){


	printk("Device released\n");
	
	return 0;
}

/* 
 * Called when a process, which already opened the dev file, attempts
 * to read from it.
 */
static ssize_t device_read(struct file *filp, /* see include/linux/fs.h*/
			   char *buffer,      /* buffer to fill with
						 data */
			   size_t length,     /* length of the
						 buffer  */
			   loff_t *offset)
{

	
   /*
   * Number of bytes actually written to the buffer
   */
   int  bytes_read = 0;
   char readChar = 0;

  
  /* 
   * Actually put the data into the buffer
   */
  int i = 0;
  while (i < length) {
    
    /* 
     * The buffer is in the user data segment, not the kernel segment
     * so "*" assignment won't work.  We have to use put_user which
     * copies data from the kernel data segment to the user data
     * segment.
     */
    readChar = ioread8(virt_addr + i);
    put_user(readChar, buffer++); /* one char at a time... */
    
    i++;
    bytes_read++;
  }
  
  /* 
   * Most read functions return the number of bytes put into the
   * buffer
   */
  return bytes_read;
}

/*  
 * called when a process writes to dev file:
 * 
 */
static ssize_t
device_write(struct file *filp, const char *buff, size_t len, loff_t *off)
{

        int bytes_write;
	int k;
        bytes_write = 0;
	if(len < 0 || buff == NULL){
	        printk(KERN_ALERT "Invalid arguments passed for device_write method\n");
		return -1;
	}

	for( k = 0 ; k < len ; k++ ){

		
                get_user(writeChar,buff + k);
		// write this char to its correct location 
		iowrite8(writeChar,virt_addr + k );
		bytes_write++;

	     		

	}

	return bytes_write;

}

// these define info that can be displayed by modinfo

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ECEN749 Amarnath and Gabriel");
MODULE_DESCRIPTION("Simple multiplier module");

// declare which functions we want to use for initialization and exit
module_init(my_init);
module_exit(my_exit);

