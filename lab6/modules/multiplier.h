// device open, release, read, write prototypes

// device open 
static int device_open(struct inode *inode, struct file *file);

// device release 
static int device_release(struct inode *inode, struct file *file);

// device read
static ssize_t device_read(struct file *, char *, size_t, loff_t *);

// device write
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);


// defines to be used in multiplier.c
#define DEVICE_NAME "multiplier"
#define BUF_LEN 12
/* 
 * Global variables are declared as static, so are global but only
 * accessible within the file.
 */
static int Major;		/* Major number assigned to our device
				   driver */
//static int Device_Open = 0;	/* Flag to signify open device */
//static char msg[BUF_LEN];	/* The msg the device will give when
//				   asked */
//static char *msg_Ptr;


