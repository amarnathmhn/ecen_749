`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:06:35 01/29/2016 
// Design Name: 
// Module Name:    switch 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module switch(
    input [3:0] SWITCHES,
    output [3:0] LEDS
    );

// LEDS glow as dictated by switches.
assign LEDS[3:0] = SWITCHES[3:0];

endmodule
