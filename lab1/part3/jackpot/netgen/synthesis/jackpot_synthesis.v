////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2008 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: K.39
//  \   \         Application: netgen
//  /   /         Filename: jackpot_synthesis.v
// /___/   /\     Timestamp: Wed Feb  3 19:24:00 2016
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -intstyle ise -insert_glbl true -w -dir netgen/synthesis -ofmt verilog -sim jackpot.ngc jackpot_synthesis.v 
// Device	: xc5vlx110t-1-ff1136
// Input file	: jackpot.ngc
// Output file	: /homes/grad/amarnathmhn/ECEN_749/lab1/part3/jackpot/netgen/synthesis/jackpot_synthesis.v
// # of Modules	: 1
// Design Name	: jackpot
// Xilinx        : /softwares/Linux/xilinx/10.1/ISE
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Development System Reference Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module jackpot (
  RESET, CLOCK, LEDS, SWITCHES
);
  input RESET;
  input CLOCK;
  output [3 : 0] LEDS;
  input [3 : 0] SWITCHES;
  wire CLOCK_BUFGP_1;
  wire LEDS_0_6;
  wire LEDS_1_7;
  wire LEDS_2_8;
  wire LEDS_3_9;
  wire LEDS_CONT_and0000;
  wire LEDS_not0001;
  wire \Mcount_edgeCounter_cy<10>_rt_22 ;
  wire \Mcount_edgeCounter_cy<11>_rt_24 ;
  wire \Mcount_edgeCounter_cy<12>_rt_26 ;
  wire \Mcount_edgeCounter_cy<13>_rt_28 ;
  wire \Mcount_edgeCounter_cy<14>_rt_30 ;
  wire \Mcount_edgeCounter_cy<15>_rt_32 ;
  wire \Mcount_edgeCounter_cy<16>_rt_34 ;
  wire \Mcount_edgeCounter_cy<17>_rt_36 ;
  wire \Mcount_edgeCounter_cy<18>_rt_38 ;
  wire \Mcount_edgeCounter_cy<19>_rt_40 ;
  wire \Mcount_edgeCounter_cy<1>_rt_42 ;
  wire \Mcount_edgeCounter_cy<20>_rt_44 ;
  wire \Mcount_edgeCounter_cy<21>_rt_46 ;
  wire \Mcount_edgeCounter_cy<22>_rt_48 ;
  wire \Mcount_edgeCounter_cy<23>_rt_50 ;
  wire \Mcount_edgeCounter_cy<24>_rt_52 ;
  wire \Mcount_edgeCounter_cy<2>_rt_54 ;
  wire \Mcount_edgeCounter_cy<3>_rt_56 ;
  wire \Mcount_edgeCounter_cy<4>_rt_58 ;
  wire \Mcount_edgeCounter_cy<5>_rt_60 ;
  wire \Mcount_edgeCounter_cy<6>_rt_62 ;
  wire \Mcount_edgeCounter_cy<7>_rt_64 ;
  wire \Mcount_edgeCounter_cy<8>_rt_66 ;
  wire \Mcount_edgeCounter_cy<9>_rt_68 ;
  wire \Mcount_edgeCounter_xor<25>_rt_70 ;
  wire N11;
  wire N13;
  wire N14;
  wire N16;
  wire N2;
  wire N3;
  wire RESET_IBUF_78;
  wire SWITCHES_0_IBUF_109;
  wire SWITCHES_1_IBUF_110;
  wire SWITCHES_2_IBUF_111;
  wire SWITCHES_3_IBUF_112;
  wire edgeCounter_0_rstpot_114;
  wire edgeCounter_10_rstpot_117;
  wire edgeCounter_11_rstpot_119;
  wire edgeCounter_12_rstpot_121;
  wire edgeCounter_13_rstpot_123;
  wire edgeCounter_14_rstpot_125;
  wire edgeCounter_15_rstpot_127;
  wire edgeCounter_16_rstpot_129;
  wire edgeCounter_17_rstpot_131;
  wire edgeCounter_18_rstpot_133;
  wire edgeCounter_19_rstpot_135;
  wire edgeCounter_1_rstpot_136;
  wire edgeCounter_20_rstpot_139;
  wire edgeCounter_21_rstpot_141;
  wire edgeCounter_22_rstpot_143;
  wire edgeCounter_23_rstpot_145;
  wire edgeCounter_24_rstpot_147;
  wire edgeCounter_25_rstpot_149;
  wire edgeCounter_2_rstpot_150;
  wire edgeCounter_3_rstpot_152;
  wire edgeCounter_4_rstpot_154;
  wire edgeCounter_5_rstpot_156;
  wire edgeCounter_6_rstpot_158;
  wire edgeCounter_7_rstpot_160;
  wire edgeCounter_8_rstpot_162;
  wire edgeCounter_9_rstpot_164;
  wire edgeCounter_cmp_ge0000;
  wire edgeCounter_cmp_ge0000312_166;
  wire edgeCounter_cmp_ge000033_167;
  wire edgeCounter_cmp_ge0000333_168;
  wire edgeCounter_cmp_ge0000358_169;
  wire present_state_FSM_FFd1_170;
  wire \present_state_FSM_FFd1-In_171 ;
  wire present_state_FSM_FFd2_172;
  wire \present_state_FSM_FFd2-In_173 ;
  wire slowClock_174;
  wire slowClock_not0001;
  wire [3 : 0] LEDS_CONT;
  wire [3 : 0] LEDS_mux0000;
  wire [24 : 0] Mcount_edgeCounter_cy;
  wire [0 : 0] Mcount_edgeCounter_lut;
  wire [25 : 0] Result;
  wire [25 : 0] edgeCounter;
  FDE #(
    .INIT ( 1'b0 ))
  slowClock (
    .C(CLOCK_BUFGP_1),
    .CE(edgeCounter_cmp_ge0000),
    .D(slowClock_not0001),
    .Q(slowClock_174)
  );
  GND   XST_GND (
    .G(N2)
  );
  VCC   XST_VCC (
    .P(N3)
  );
  LD   LEDS_0 (
    .D(LEDS_mux0000[0]),
    .G(LEDS_not0001),
    .Q(LEDS_0_6)
  );
  LD   LEDS_1 (
    .D(LEDS_mux0000[1]),
    .G(LEDS_not0001),
    .Q(LEDS_1_7)
  );
  LD   LEDS_2 (
    .D(LEDS_mux0000[2]),
    .G(LEDS_not0001),
    .Q(LEDS_2_8)
  );
  LD   LEDS_3 (
    .D(LEDS_mux0000[3]),
    .G(LEDS_not0001),
    .Q(LEDS_3_9)
  );
  FDC #(
    .INIT ( 1'b0 ))
  LEDS_CONT_3 (
    .C(slowClock_174),
    .CLR(RESET_IBUF_78),
    .D(LEDS_CONT[2]),
    .Q(LEDS_CONT[3])
  );
  FDC #(
    .INIT ( 1'b0 ))
  LEDS_CONT_2 (
    .C(slowClock_174),
    .CLR(RESET_IBUF_78),
    .D(LEDS_CONT[1]),
    .Q(LEDS_CONT[2])
  );
  FDC #(
    .INIT ( 1'b0 ))
  LEDS_CONT_1 (
    .C(slowClock_174),
    .CLR(RESET_IBUF_78),
    .D(LEDS_CONT[0]),
    .Q(LEDS_CONT[1])
  );
  FDP #(
    .INIT ( 1'b0 ))
  LEDS_CONT_0 (
    .C(slowClock_174),
    .D(LEDS_CONT_and0000),
    .PRE(RESET_IBUF_78),
    .Q(LEDS_CONT[0])
  );
  MUXCY   \Mcount_edgeCounter_cy<0>  (
    .CI(N2),
    .DI(N3),
    .S(Mcount_edgeCounter_lut[0]),
    .O(Mcount_edgeCounter_cy[0])
  );
  XORCY   \Mcount_edgeCounter_xor<0>  (
    .CI(N2),
    .LI(Mcount_edgeCounter_lut[0]),
    .O(Result[0])
  );
  MUXCY   \Mcount_edgeCounter_cy<1>  (
    .CI(Mcount_edgeCounter_cy[0]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<1>_rt_42 ),
    .O(Mcount_edgeCounter_cy[1])
  );
  XORCY   \Mcount_edgeCounter_xor<1>  (
    .CI(Mcount_edgeCounter_cy[0]),
    .LI(\Mcount_edgeCounter_cy<1>_rt_42 ),
    .O(Result[1])
  );
  MUXCY   \Mcount_edgeCounter_cy<2>  (
    .CI(Mcount_edgeCounter_cy[1]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<2>_rt_54 ),
    .O(Mcount_edgeCounter_cy[2])
  );
  XORCY   \Mcount_edgeCounter_xor<2>  (
    .CI(Mcount_edgeCounter_cy[1]),
    .LI(\Mcount_edgeCounter_cy<2>_rt_54 ),
    .O(Result[2])
  );
  MUXCY   \Mcount_edgeCounter_cy<3>  (
    .CI(Mcount_edgeCounter_cy[2]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<3>_rt_56 ),
    .O(Mcount_edgeCounter_cy[3])
  );
  XORCY   \Mcount_edgeCounter_xor<3>  (
    .CI(Mcount_edgeCounter_cy[2]),
    .LI(\Mcount_edgeCounter_cy<3>_rt_56 ),
    .O(Result[3])
  );
  MUXCY   \Mcount_edgeCounter_cy<4>  (
    .CI(Mcount_edgeCounter_cy[3]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<4>_rt_58 ),
    .O(Mcount_edgeCounter_cy[4])
  );
  XORCY   \Mcount_edgeCounter_xor<4>  (
    .CI(Mcount_edgeCounter_cy[3]),
    .LI(\Mcount_edgeCounter_cy<4>_rt_58 ),
    .O(Result[4])
  );
  MUXCY   \Mcount_edgeCounter_cy<5>  (
    .CI(Mcount_edgeCounter_cy[4]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<5>_rt_60 ),
    .O(Mcount_edgeCounter_cy[5])
  );
  XORCY   \Mcount_edgeCounter_xor<5>  (
    .CI(Mcount_edgeCounter_cy[4]),
    .LI(\Mcount_edgeCounter_cy<5>_rt_60 ),
    .O(Result[5])
  );
  MUXCY   \Mcount_edgeCounter_cy<6>  (
    .CI(Mcount_edgeCounter_cy[5]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<6>_rt_62 ),
    .O(Mcount_edgeCounter_cy[6])
  );
  XORCY   \Mcount_edgeCounter_xor<6>  (
    .CI(Mcount_edgeCounter_cy[5]),
    .LI(\Mcount_edgeCounter_cy<6>_rt_62 ),
    .O(Result[6])
  );
  MUXCY   \Mcount_edgeCounter_cy<7>  (
    .CI(Mcount_edgeCounter_cy[6]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<7>_rt_64 ),
    .O(Mcount_edgeCounter_cy[7])
  );
  XORCY   \Mcount_edgeCounter_xor<7>  (
    .CI(Mcount_edgeCounter_cy[6]),
    .LI(\Mcount_edgeCounter_cy<7>_rt_64 ),
    .O(Result[7])
  );
  MUXCY   \Mcount_edgeCounter_cy<8>  (
    .CI(Mcount_edgeCounter_cy[7]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<8>_rt_66 ),
    .O(Mcount_edgeCounter_cy[8])
  );
  XORCY   \Mcount_edgeCounter_xor<8>  (
    .CI(Mcount_edgeCounter_cy[7]),
    .LI(\Mcount_edgeCounter_cy<8>_rt_66 ),
    .O(Result[8])
  );
  MUXCY   \Mcount_edgeCounter_cy<9>  (
    .CI(Mcount_edgeCounter_cy[8]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<9>_rt_68 ),
    .O(Mcount_edgeCounter_cy[9])
  );
  XORCY   \Mcount_edgeCounter_xor<9>  (
    .CI(Mcount_edgeCounter_cy[8]),
    .LI(\Mcount_edgeCounter_cy<9>_rt_68 ),
    .O(Result[9])
  );
  MUXCY   \Mcount_edgeCounter_cy<10>  (
    .CI(Mcount_edgeCounter_cy[9]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<10>_rt_22 ),
    .O(Mcount_edgeCounter_cy[10])
  );
  XORCY   \Mcount_edgeCounter_xor<10>  (
    .CI(Mcount_edgeCounter_cy[9]),
    .LI(\Mcount_edgeCounter_cy<10>_rt_22 ),
    .O(Result[10])
  );
  MUXCY   \Mcount_edgeCounter_cy<11>  (
    .CI(Mcount_edgeCounter_cy[10]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<11>_rt_24 ),
    .O(Mcount_edgeCounter_cy[11])
  );
  XORCY   \Mcount_edgeCounter_xor<11>  (
    .CI(Mcount_edgeCounter_cy[10]),
    .LI(\Mcount_edgeCounter_cy<11>_rt_24 ),
    .O(Result[11])
  );
  MUXCY   \Mcount_edgeCounter_cy<12>  (
    .CI(Mcount_edgeCounter_cy[11]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<12>_rt_26 ),
    .O(Mcount_edgeCounter_cy[12])
  );
  XORCY   \Mcount_edgeCounter_xor<12>  (
    .CI(Mcount_edgeCounter_cy[11]),
    .LI(\Mcount_edgeCounter_cy<12>_rt_26 ),
    .O(Result[12])
  );
  MUXCY   \Mcount_edgeCounter_cy<13>  (
    .CI(Mcount_edgeCounter_cy[12]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<13>_rt_28 ),
    .O(Mcount_edgeCounter_cy[13])
  );
  XORCY   \Mcount_edgeCounter_xor<13>  (
    .CI(Mcount_edgeCounter_cy[12]),
    .LI(\Mcount_edgeCounter_cy<13>_rt_28 ),
    .O(Result[13])
  );
  MUXCY   \Mcount_edgeCounter_cy<14>  (
    .CI(Mcount_edgeCounter_cy[13]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<14>_rt_30 ),
    .O(Mcount_edgeCounter_cy[14])
  );
  XORCY   \Mcount_edgeCounter_xor<14>  (
    .CI(Mcount_edgeCounter_cy[13]),
    .LI(\Mcount_edgeCounter_cy<14>_rt_30 ),
    .O(Result[14])
  );
  MUXCY   \Mcount_edgeCounter_cy<15>  (
    .CI(Mcount_edgeCounter_cy[14]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<15>_rt_32 ),
    .O(Mcount_edgeCounter_cy[15])
  );
  XORCY   \Mcount_edgeCounter_xor<15>  (
    .CI(Mcount_edgeCounter_cy[14]),
    .LI(\Mcount_edgeCounter_cy<15>_rt_32 ),
    .O(Result[15])
  );
  MUXCY   \Mcount_edgeCounter_cy<16>  (
    .CI(Mcount_edgeCounter_cy[15]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<16>_rt_34 ),
    .O(Mcount_edgeCounter_cy[16])
  );
  XORCY   \Mcount_edgeCounter_xor<16>  (
    .CI(Mcount_edgeCounter_cy[15]),
    .LI(\Mcount_edgeCounter_cy<16>_rt_34 ),
    .O(Result[16])
  );
  MUXCY   \Mcount_edgeCounter_cy<17>  (
    .CI(Mcount_edgeCounter_cy[16]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<17>_rt_36 ),
    .O(Mcount_edgeCounter_cy[17])
  );
  XORCY   \Mcount_edgeCounter_xor<17>  (
    .CI(Mcount_edgeCounter_cy[16]),
    .LI(\Mcount_edgeCounter_cy<17>_rt_36 ),
    .O(Result[17])
  );
  MUXCY   \Mcount_edgeCounter_cy<18>  (
    .CI(Mcount_edgeCounter_cy[17]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<18>_rt_38 ),
    .O(Mcount_edgeCounter_cy[18])
  );
  XORCY   \Mcount_edgeCounter_xor<18>  (
    .CI(Mcount_edgeCounter_cy[17]),
    .LI(\Mcount_edgeCounter_cy<18>_rt_38 ),
    .O(Result[18])
  );
  MUXCY   \Mcount_edgeCounter_cy<19>  (
    .CI(Mcount_edgeCounter_cy[18]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<19>_rt_40 ),
    .O(Mcount_edgeCounter_cy[19])
  );
  XORCY   \Mcount_edgeCounter_xor<19>  (
    .CI(Mcount_edgeCounter_cy[18]),
    .LI(\Mcount_edgeCounter_cy<19>_rt_40 ),
    .O(Result[19])
  );
  MUXCY   \Mcount_edgeCounter_cy<20>  (
    .CI(Mcount_edgeCounter_cy[19]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<20>_rt_44 ),
    .O(Mcount_edgeCounter_cy[20])
  );
  XORCY   \Mcount_edgeCounter_xor<20>  (
    .CI(Mcount_edgeCounter_cy[19]),
    .LI(\Mcount_edgeCounter_cy<20>_rt_44 ),
    .O(Result[20])
  );
  MUXCY   \Mcount_edgeCounter_cy<21>  (
    .CI(Mcount_edgeCounter_cy[20]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<21>_rt_46 ),
    .O(Mcount_edgeCounter_cy[21])
  );
  XORCY   \Mcount_edgeCounter_xor<21>  (
    .CI(Mcount_edgeCounter_cy[20]),
    .LI(\Mcount_edgeCounter_cy<21>_rt_46 ),
    .O(Result[21])
  );
  MUXCY   \Mcount_edgeCounter_cy<22>  (
    .CI(Mcount_edgeCounter_cy[21]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<22>_rt_48 ),
    .O(Mcount_edgeCounter_cy[22])
  );
  XORCY   \Mcount_edgeCounter_xor<22>  (
    .CI(Mcount_edgeCounter_cy[21]),
    .LI(\Mcount_edgeCounter_cy<22>_rt_48 ),
    .O(Result[22])
  );
  MUXCY   \Mcount_edgeCounter_cy<23>  (
    .CI(Mcount_edgeCounter_cy[22]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<23>_rt_50 ),
    .O(Mcount_edgeCounter_cy[23])
  );
  XORCY   \Mcount_edgeCounter_xor<23>  (
    .CI(Mcount_edgeCounter_cy[22]),
    .LI(\Mcount_edgeCounter_cy<23>_rt_50 ),
    .O(Result[23])
  );
  MUXCY   \Mcount_edgeCounter_cy<24>  (
    .CI(Mcount_edgeCounter_cy[23]),
    .DI(N2),
    .S(\Mcount_edgeCounter_cy<24>_rt_52 ),
    .O(Mcount_edgeCounter_cy[24])
  );
  XORCY   \Mcount_edgeCounter_xor<24>  (
    .CI(Mcount_edgeCounter_cy[23]),
    .LI(\Mcount_edgeCounter_cy<24>_rt_52 ),
    .O(Result[24])
  );
  XORCY   \Mcount_edgeCounter_xor<25>  (
    .CI(Mcount_edgeCounter_cy[24]),
    .LI(\Mcount_edgeCounter_xor<25>_rt_70 ),
    .O(Result[25])
  );
  FDC #(
    .INIT ( 1'b0 ))
  present_state_FSM_FFd1 (
    .C(slowClock_174),
    .CLR(RESET_IBUF_78),
    .D(\present_state_FSM_FFd1-In_171 ),
    .Q(present_state_FSM_FFd1_170)
  );
  FDC #(
    .INIT ( 1'b0 ))
  present_state_FSM_FFd2 (
    .C(slowClock_174),
    .CLR(RESET_IBUF_78),
    .D(\present_state_FSM_FFd2-In_173 ),
    .Q(present_state_FSM_FFd2_172)
  );
  LUT4 #(
    .INIT ( 16'h0100 ))
  LEDS_CONT_and00001 (
    .I0(LEDS_CONT[0]),
    .I1(LEDS_CONT[1]),
    .I2(LEDS_CONT[2]),
    .I3(LEDS_CONT[3]),
    .O(LEDS_CONT_and0000)
  );
  LUT3 #(
    .INIT ( 8'h01 ))
  \present_state_FSM_FFd1-In311  (
    .I0(SWITCHES_2_IBUF_111),
    .I1(SWITCHES_3_IBUF_112),
    .I2(SWITCHES_1_IBUF_110),
    .O(N11)
  );
  LUT6 #(
    .INIT ( 64'hFEFEFEFEFFF3CFC3 ))
  \present_state_FSM_FFd1-In_SW0  (
    .I0(LEDS_0_6),
    .I1(SWITCHES_2_IBUF_111),
    .I2(SWITCHES_1_IBUF_110),
    .I3(LEDS_2_8),
    .I4(LEDS_1_7),
    .I5(SWITCHES_0_IBUF_109),
    .O(N13)
  );
  LUT3 #(
    .INIT ( 8'hFE ))
  \present_state_FSM_FFd1-In_SW1  (
    .I0(SWITCHES_2_IBUF_111),
    .I1(SWITCHES_1_IBUF_110),
    .I2(SWITCHES_0_IBUF_109),
    .O(N14)
  );
  LUT6 #(
    .INIT ( 64'hAA88AA8DAB88AB8D ))
  \present_state_FSM_FFd1-In  (
    .I0(present_state_FSM_FFd1_170),
    .I1(N14),
    .I2(present_state_FSM_FFd2_172),
    .I3(SWITCHES_3_IBUF_112),
    .I4(N13),
    .I5(LEDS_3_9),
    .O(\present_state_FSM_FFd1-In_171 )
  );
  LUT5 #(
    .INIT ( 32'hFFFF8AAA ))
  \LEDS_mux0000<3>1  (
    .I0(LEDS_CONT[3]),
    .I1(SWITCHES_0_IBUF_109),
    .I2(N11),
    .I3(present_state_FSM_FFd1_170),
    .I4(\present_state_FSM_FFd2-In_173 ),
    .O(LEDS_mux0000[3])
  );
  LUT5 #(
    .INIT ( 32'hFFFF8AAA ))
  \LEDS_mux0000<2>1  (
    .I0(LEDS_CONT[2]),
    .I1(SWITCHES_0_IBUF_109),
    .I2(N11),
    .I3(present_state_FSM_FFd1_170),
    .I4(\present_state_FSM_FFd2-In_173 ),
    .O(LEDS_mux0000[2])
  );
  LUT5 #(
    .INIT ( 32'hFFFF8AAA ))
  \LEDS_mux0000<1>1  (
    .I0(LEDS_CONT[1]),
    .I1(SWITCHES_0_IBUF_109),
    .I2(N11),
    .I3(present_state_FSM_FFd1_170),
    .I4(\present_state_FSM_FFd2-In_173 ),
    .O(LEDS_mux0000[1])
  );
  LUT5 #(
    .INIT ( 32'hFFFF8AAA ))
  \LEDS_mux0000<0>1  (
    .I0(LEDS_CONT[0]),
    .I1(SWITCHES_0_IBUF_109),
    .I2(N11),
    .I3(present_state_FSM_FFd1_170),
    .I4(\present_state_FSM_FFd2-In_173 ),
    .O(LEDS_mux0000[0])
  );
  LUT6 #(
    .INIT ( 64'hE9F9EDFDEBFBEFFF ))
  \present_state_FSM_FFd2-In_SW0  (
    .I0(SWITCHES_0_IBUF_109),
    .I1(SWITCHES_1_IBUF_110),
    .I2(SWITCHES_2_IBUF_111),
    .I3(LEDS_2_8),
    .I4(LEDS_1_7),
    .I5(LEDS_0_6),
    .O(N16)
  );
  LUT6 #(
    .INIT ( 64'hF0F4F0F0F0F7F0F3 ))
  \present_state_FSM_FFd2-In  (
    .I0(N14),
    .I1(SWITCHES_3_IBUF_112),
    .I2(present_state_FSM_FFd2_172),
    .I3(present_state_FSM_FFd1_170),
    .I4(LEDS_3_9),
    .I5(N16),
    .O(\present_state_FSM_FFd2-In_173 )
  );
  LUT3 #(
    .INIT ( 8'hFE ))
  edgeCounter_cmp_ge000033 (
    .I0(edgeCounter[8]),
    .I1(edgeCounter[9]),
    .I2(edgeCounter[10]),
    .O(edgeCounter_cmp_ge000033_167)
  );
  LUT4 #(
    .INIT ( 16'h8000 ))
  edgeCounter_cmp_ge0000312 (
    .I0(edgeCounter[12]),
    .I1(edgeCounter[13]),
    .I2(edgeCounter[14]),
    .I3(edgeCounter[15]),
    .O(edgeCounter_cmp_ge0000312_166)
  );
  LUT5 #(
    .INIT ( 32'h80000000 ))
  edgeCounter_cmp_ge0000358 (
    .I0(edgeCounter[20]),
    .I1(edgeCounter[21]),
    .I2(edgeCounter[22]),
    .I3(edgeCounter[23]),
    .I4(edgeCounter[19]),
    .O(edgeCounter_cmp_ge0000358_169)
  );
  LUT5 #(
    .INIT ( 32'hF0A0E0A0 ))
  edgeCounter_cmp_ge0000384 (
    .I0(edgeCounter[24]),
    .I1(edgeCounter[18]),
    .I2(edgeCounter[25]),
    .I3(edgeCounter_cmp_ge0000358_169),
    .I4(edgeCounter_cmp_ge0000333_168),
    .O(edgeCounter_cmp_ge0000)
  );
  IBUF   RESET_IBUF (
    .I(RESET),
    .O(RESET_IBUF_78)
  );
  IBUF   SWITCHES_3_IBUF (
    .I(SWITCHES[3]),
    .O(SWITCHES_3_IBUF_112)
  );
  IBUF   SWITCHES_2_IBUF (
    .I(SWITCHES[2]),
    .O(SWITCHES_2_IBUF_111)
  );
  IBUF   SWITCHES_1_IBUF (
    .I(SWITCHES[1]),
    .O(SWITCHES_1_IBUF_110)
  );
  IBUF   SWITCHES_0_IBUF (
    .I(SWITCHES[0]),
    .O(SWITCHES_0_IBUF_109)
  );
  OBUF   LEDS_3_OBUF (
    .I(LEDS_3_9),
    .O(LEDS[3])
  );
  OBUF   LEDS_2_OBUF (
    .I(LEDS_2_8),
    .O(LEDS[2])
  );
  OBUF   LEDS_1_OBUF (
    .I(LEDS_1_7),
    .O(LEDS[1])
  );
  OBUF   LEDS_0_OBUF (
    .I(LEDS_0_6),
    .O(LEDS[0])
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<1>_rt  (
    .I0(edgeCounter[1]),
    .O(\Mcount_edgeCounter_cy<1>_rt_42 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<2>_rt  (
    .I0(edgeCounter[2]),
    .O(\Mcount_edgeCounter_cy<2>_rt_54 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<3>_rt  (
    .I0(edgeCounter[3]),
    .O(\Mcount_edgeCounter_cy<3>_rt_56 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<4>_rt  (
    .I0(edgeCounter[4]),
    .O(\Mcount_edgeCounter_cy<4>_rt_58 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<5>_rt  (
    .I0(edgeCounter[5]),
    .O(\Mcount_edgeCounter_cy<5>_rt_60 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<6>_rt  (
    .I0(edgeCounter[6]),
    .O(\Mcount_edgeCounter_cy<6>_rt_62 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<7>_rt  (
    .I0(edgeCounter[7]),
    .O(\Mcount_edgeCounter_cy<7>_rt_64 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<8>_rt  (
    .I0(edgeCounter[8]),
    .O(\Mcount_edgeCounter_cy<8>_rt_66 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<9>_rt  (
    .I0(edgeCounter[9]),
    .O(\Mcount_edgeCounter_cy<9>_rt_68 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<10>_rt  (
    .I0(edgeCounter[10]),
    .O(\Mcount_edgeCounter_cy<10>_rt_22 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<11>_rt  (
    .I0(edgeCounter[11]),
    .O(\Mcount_edgeCounter_cy<11>_rt_24 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<12>_rt  (
    .I0(edgeCounter[12]),
    .O(\Mcount_edgeCounter_cy<12>_rt_26 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<13>_rt  (
    .I0(edgeCounter[13]),
    .O(\Mcount_edgeCounter_cy<13>_rt_28 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<14>_rt  (
    .I0(edgeCounter[14]),
    .O(\Mcount_edgeCounter_cy<14>_rt_30 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<15>_rt  (
    .I0(edgeCounter[15]),
    .O(\Mcount_edgeCounter_cy<15>_rt_32 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<16>_rt  (
    .I0(edgeCounter[16]),
    .O(\Mcount_edgeCounter_cy<16>_rt_34 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<17>_rt  (
    .I0(edgeCounter[17]),
    .O(\Mcount_edgeCounter_cy<17>_rt_36 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<18>_rt  (
    .I0(edgeCounter[18]),
    .O(\Mcount_edgeCounter_cy<18>_rt_38 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<19>_rt  (
    .I0(edgeCounter[19]),
    .O(\Mcount_edgeCounter_cy<19>_rt_40 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<20>_rt  (
    .I0(edgeCounter[20]),
    .O(\Mcount_edgeCounter_cy<20>_rt_44 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<21>_rt  (
    .I0(edgeCounter[21]),
    .O(\Mcount_edgeCounter_cy<21>_rt_46 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<22>_rt  (
    .I0(edgeCounter[22]),
    .O(\Mcount_edgeCounter_cy<22>_rt_48 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<23>_rt  (
    .I0(edgeCounter[23]),
    .O(\Mcount_edgeCounter_cy<23>_rt_50 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_cy<24>_rt  (
    .I0(edgeCounter[24]),
    .O(\Mcount_edgeCounter_cy<24>_rt_52 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_edgeCounter_xor<25>_rt  (
    .I0(edgeCounter[25]),
    .O(\Mcount_edgeCounter_xor<25>_rt_70 )
  );
  LUT6 #(
    .INIT ( 64'hFF00FE00AA00AA00 ))
  edgeCounter_cmp_ge0000333 (
    .I0(edgeCounter[16]),
    .I1(edgeCounter[7]),
    .I2(edgeCounter[11]),
    .I3(edgeCounter[17]),
    .I4(edgeCounter_cmp_ge000033_167),
    .I5(edgeCounter_cmp_ge0000312_166),
    .O(edgeCounter_cmp_ge0000333_168)
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_0 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_0_rstpot_114),
    .Q(edgeCounter[0])
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_1 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_1_rstpot_136),
    .Q(edgeCounter[1])
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_2 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_2_rstpot_150),
    .Q(edgeCounter[2])
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_3 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_3_rstpot_152),
    .Q(edgeCounter[3])
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_4 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_4_rstpot_154),
    .Q(edgeCounter[4])
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_5 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_5_rstpot_156),
    .Q(edgeCounter[5])
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_6 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_6_rstpot_158),
    .Q(edgeCounter[6])
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_7 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_7_rstpot_160),
    .Q(edgeCounter[7])
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_8 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_8_rstpot_162),
    .Q(edgeCounter[8])
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_9 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_9_rstpot_164),
    .Q(edgeCounter[9])
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_10 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_10_rstpot_117),
    .Q(edgeCounter[10])
  );
  LUT6 #(
    .INIT ( 64'h5500770057007700 ))
  edgeCounter_0_rstpot (
    .I0(edgeCounter[25]),
    .I1(edgeCounter[24]),
    .I2(edgeCounter[18]),
    .I3(Result[0]),
    .I4(edgeCounter_cmp_ge0000358_169),
    .I5(edgeCounter_cmp_ge0000333_168),
    .O(edgeCounter_0_rstpot_114)
  );
  LUT6 #(
    .INIT ( 64'h5577000057770000 ))
  edgeCounter_1_rstpot (
    .I0(edgeCounter[25]),
    .I1(edgeCounter[24]),
    .I2(edgeCounter[18]),
    .I3(edgeCounter_cmp_ge0000358_169),
    .I4(Result[1]),
    .I5(edgeCounter_cmp_ge0000333_168),
    .O(edgeCounter_1_rstpot_136)
  );
  LUT6 #(
    .INIT ( 64'h5577000057770000 ))
  edgeCounter_2_rstpot (
    .I0(edgeCounter[25]),
    .I1(edgeCounter[24]),
    .I2(edgeCounter[18]),
    .I3(edgeCounter_cmp_ge0000358_169),
    .I4(Result[2]),
    .I5(edgeCounter_cmp_ge0000333_168),
    .O(edgeCounter_2_rstpot_150)
  );
  LUT6 #(
    .INIT ( 64'h5577000057770000 ))
  edgeCounter_3_rstpot (
    .I0(edgeCounter[25]),
    .I1(edgeCounter[24]),
    .I2(edgeCounter[18]),
    .I3(edgeCounter_cmp_ge0000358_169),
    .I4(Result[3]),
    .I5(edgeCounter_cmp_ge0000333_168),
    .O(edgeCounter_3_rstpot_152)
  );
  LUT6 #(
    .INIT ( 64'h5577000057770000 ))
  edgeCounter_4_rstpot (
    .I0(edgeCounter[25]),
    .I1(edgeCounter[24]),
    .I2(edgeCounter[18]),
    .I3(edgeCounter_cmp_ge0000358_169),
    .I4(Result[4]),
    .I5(edgeCounter_cmp_ge0000333_168),
    .O(edgeCounter_4_rstpot_154)
  );
  LUT6 #(
    .INIT ( 64'h5577000057770000 ))
  edgeCounter_5_rstpot (
    .I0(edgeCounter[25]),
    .I1(edgeCounter[24]),
    .I2(edgeCounter[18]),
    .I3(edgeCounter_cmp_ge0000358_169),
    .I4(Result[5]),
    .I5(edgeCounter_cmp_ge0000333_168),
    .O(edgeCounter_5_rstpot_156)
  );
  LUT6 #(
    .INIT ( 64'h5577000057770000 ))
  edgeCounter_6_rstpot (
    .I0(edgeCounter[25]),
    .I1(edgeCounter[24]),
    .I2(edgeCounter[18]),
    .I3(edgeCounter_cmp_ge0000358_169),
    .I4(Result[6]),
    .I5(edgeCounter_cmp_ge0000333_168),
    .O(edgeCounter_6_rstpot_158)
  );
  LUT6 #(
    .INIT ( 64'h5577000057770000 ))
  edgeCounter_7_rstpot (
    .I0(edgeCounter[25]),
    .I1(edgeCounter[24]),
    .I2(edgeCounter[18]),
    .I3(edgeCounter_cmp_ge0000358_169),
    .I4(Result[7]),
    .I5(edgeCounter_cmp_ge0000333_168),
    .O(edgeCounter_7_rstpot_160)
  );
  LUT6 #(
    .INIT ( 64'h5577000057770000 ))
  edgeCounter_8_rstpot (
    .I0(edgeCounter[25]),
    .I1(edgeCounter[24]),
    .I2(edgeCounter[18]),
    .I3(edgeCounter_cmp_ge0000358_169),
    .I4(Result[8]),
    .I5(edgeCounter_cmp_ge0000333_168),
    .O(edgeCounter_8_rstpot_162)
  );
  LUT6 #(
    .INIT ( 64'h5577000057770000 ))
  edgeCounter_9_rstpot (
    .I0(edgeCounter[25]),
    .I1(edgeCounter[24]),
    .I2(edgeCounter[18]),
    .I3(edgeCounter_cmp_ge0000358_169),
    .I4(Result[9]),
    .I5(edgeCounter_cmp_ge0000333_168),
    .O(edgeCounter_9_rstpot_164)
  );
  LUT6 #(
    .INIT ( 64'h5577000057770000 ))
  edgeCounter_10_rstpot (
    .I0(edgeCounter[25]),
    .I1(edgeCounter[24]),
    .I2(edgeCounter[18]),
    .I3(edgeCounter_cmp_ge0000358_169),
    .I4(Result[10]),
    .I5(edgeCounter_cmp_ge0000333_168),
    .O(edgeCounter_10_rstpot_117)
  );
  LUT5 #(
    .INIT ( 32'hFFFFFFFD ))
  LEDS_not00011 (
    .I0(present_state_FSM_FFd1_170),
    .I1(SWITCHES_0_IBUF_109),
    .I2(SWITCHES_2_IBUF_111),
    .I3(SWITCHES_3_IBUF_112),
    .I4(SWITCHES_1_IBUF_110),
    .O(LEDS_not0001)
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_11 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_11_rstpot_119),
    .Q(edgeCounter[11])
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_12 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_12_rstpot_121),
    .Q(edgeCounter[12])
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  edgeCounter_13_rstpot (
    .I0(Result[13]),
    .I1(edgeCounter_cmp_ge0000),
    .O(edgeCounter_13_rstpot_123)
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_13 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_13_rstpot_123),
    .Q(edgeCounter[13])
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  edgeCounter_14_rstpot (
    .I0(Result[14]),
    .I1(edgeCounter_cmp_ge0000),
    .O(edgeCounter_14_rstpot_125)
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_14 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_14_rstpot_125),
    .Q(edgeCounter[14])
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  edgeCounter_15_rstpot (
    .I0(Result[15]),
    .I1(edgeCounter_cmp_ge0000),
    .O(edgeCounter_15_rstpot_127)
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_15 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_15_rstpot_127),
    .Q(edgeCounter[15])
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  edgeCounter_16_rstpot (
    .I0(Result[16]),
    .I1(edgeCounter_cmp_ge0000),
    .O(edgeCounter_16_rstpot_129)
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_16 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_16_rstpot_129),
    .Q(edgeCounter[16])
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  edgeCounter_17_rstpot (
    .I0(Result[17]),
    .I1(edgeCounter_cmp_ge0000),
    .O(edgeCounter_17_rstpot_131)
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_17 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_17_rstpot_131),
    .Q(edgeCounter[17])
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  edgeCounter_18_rstpot (
    .I0(Result[18]),
    .I1(edgeCounter_cmp_ge0000),
    .O(edgeCounter_18_rstpot_133)
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_18 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_18_rstpot_133),
    .Q(edgeCounter[18])
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  edgeCounter_19_rstpot (
    .I0(Result[19]),
    .I1(edgeCounter_cmp_ge0000),
    .O(edgeCounter_19_rstpot_135)
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_19 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_19_rstpot_135),
    .Q(edgeCounter[19])
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  edgeCounter_20_rstpot (
    .I0(Result[20]),
    .I1(edgeCounter_cmp_ge0000),
    .O(edgeCounter_20_rstpot_139)
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_20 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_20_rstpot_139),
    .Q(edgeCounter[20])
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  edgeCounter_21_rstpot (
    .I0(Result[21]),
    .I1(edgeCounter_cmp_ge0000),
    .O(edgeCounter_21_rstpot_141)
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_21 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_21_rstpot_141),
    .Q(edgeCounter[21])
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  edgeCounter_22_rstpot (
    .I0(Result[22]),
    .I1(edgeCounter_cmp_ge0000),
    .O(edgeCounter_22_rstpot_143)
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_22 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_22_rstpot_143),
    .Q(edgeCounter[22])
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  edgeCounter_23_rstpot (
    .I0(Result[23]),
    .I1(edgeCounter_cmp_ge0000),
    .O(edgeCounter_23_rstpot_145)
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_23 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_23_rstpot_145),
    .Q(edgeCounter[23])
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  edgeCounter_24_rstpot (
    .I0(Result[24]),
    .I1(edgeCounter_cmp_ge0000),
    .O(edgeCounter_24_rstpot_147)
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_24 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_24_rstpot_147),
    .Q(edgeCounter[24])
  );
  LUT2 #(
    .INIT ( 4'h2 ))
  edgeCounter_25_rstpot (
    .I0(Result[25]),
    .I1(edgeCounter_cmp_ge0000),
    .O(edgeCounter_25_rstpot_149)
  );
  FD #(
    .INIT ( 1'b0 ))
  edgeCounter_25 (
    .C(CLOCK_BUFGP_1),
    .D(edgeCounter_25_rstpot_149),
    .Q(edgeCounter[25])
  );
  LUT6 #(
    .INIT ( 64'h5577577700000000 ))
  edgeCounter_11_rstpot (
    .I0(edgeCounter[25]),
    .I1(edgeCounter[24]),
    .I2(edgeCounter[18]),
    .I3(edgeCounter_cmp_ge0000358_169),
    .I4(edgeCounter_cmp_ge0000333_168),
    .I5(Result[11]),
    .O(edgeCounter_11_rstpot_119)
  );
  LUT6 #(
    .INIT ( 64'h5577577700000000 ))
  edgeCounter_12_rstpot (
    .I0(edgeCounter[25]),
    .I1(edgeCounter[24]),
    .I2(edgeCounter[18]),
    .I3(edgeCounter_cmp_ge0000358_169),
    .I4(edgeCounter_cmp_ge0000333_168),
    .I5(Result[12]),
    .O(edgeCounter_12_rstpot_121)
  );
  BUFGP   CLOCK_BUFGP (
    .I(CLOCK),
    .O(CLOCK_BUFGP_1)
  );
  INV   \Mcount_edgeCounter_lut<0>_INV_0  (
    .I(edgeCounter[0]),
    .O(Mcount_edgeCounter_lut[0])
  );
  INV   slowClock_not00011_INV_0 (
    .I(slowClock_174),
    .O(slowClock_not0001)
  );
endmodule


`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

    wire GSR;
    wire GTS;
    wire PRLD;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

