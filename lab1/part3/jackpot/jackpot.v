`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:11:09 01/30/2016 
// Design Name: 
// Module Name:    jackpot 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
// Module that implements the Jackpot Game. LEDs must glow in one hot fashion. 
// SWITCH operated must match LED position to win the game.
// State Machine implementing the logic would comprise of the states :
// IDLE, LOSS, WIN;

module jackpot(
    output reg [3:0] LEDS,
    input CLOCK,
    input [3:0] SWITCHES,
    input RESET
    );

reg [25:0] edgeCounter = 26'd0;    // Register Holding the Number of Rising CLOCK Edges Counted 
reg slowClock=0;                   // slowClock that is used so that LED Blinking is visible. 
reg [3:0] LEDS_CONT=4'b0000;                // Internal Register that is continuously updated in one hot fashion

 

// The following are states of the Jackpot statemachine
parameter IDLE  = 2'b00,   // No Switch is operated. LEDs keep glowing in one hot way
          LOSS  = 2'b01,   // Jackpot LOST
          WIN   = 2'b10;	// Jackpot WON
			 
// Register that holds the state of the State Machine
reg [1:0] present_state = IDLE;	 
reg [1:0] next_state    = IDLE;

// Block that Counts the Rising CLOCK Edges of the 100MHz clock and Toggles the
// slowClock so that its period is large enough to view the glowing LED's position
always @(posedge CLOCK) begin

	 edgeCounter <= edgeCounter + 1;
	 
    if(edgeCounter >= 26'd50000000) begin
	   
		 edgeCounter <= 26'd0;		 
		 slowClock <= ~slowClock;	 		 
		
	 end

end

// Define RESET logic. 
// Assign present_state to next_state.
// Shift left LED_CONT to make it glow as per one-hot fashion
always @(posedge slowClock or posedge RESET) begin

  if(RESET) begin 
      present_state <= IDLE;
		LEDS_CONT      <= 4'b0001;
  end
  else begin
      present_state <= next_state;
		if(LEDS_CONT != 4'b1000) begin
		   LEDS_CONT   <= LEDS_CONT << 1;
		end
		else begin
		   LEDS_CONT  <= 4'b0001;
		end
  end
      
end

// Block where switch activity is detected and State is changed accordingly
// next-state and output logic is defined in this block
always @(SWITCHES) begin

next_state <= present_state;

	   case(present_state)
		  // Be in IDLE State until a LOSS or WIN has occured
		  IDLE: begin
		  // Check if a SWITCH has been operated. If So check if the Switch 
		  // Configuration is Valid and Check if it matches the LED glowing in that cycle.
		      if(SWITCHES == 1 || SWITCHES == 2 || SWITCHES == 4 || SWITCHES == 8) begin
		               if(SWITCHES[3] && LEDS[3]) begin
                         LEDS <= 4'b1111;
                          next_state <= WIN;
                         end
                        else if (SWITCHES[2] && LEDS[2])begin
                         LEDS <= 4'b1111;
                          next_state <= WIN;
                         end
                        else if (SWITCHES[1] && LEDS[1]) begin
                         LEDS <= 4'b1111;
                          next_state <= WIN;
                         end
                        else if (SWITCHES[0] && LEDS[0])begin
                         LEDS <= 4'b1111;
                          next_state <= WIN;
                        end  
                        else begin
								  // Lost the game, Move to LOSS state
                          next_state <= LOSS;     
                          LEDS <= LEDS_CONT;			  
								end
			  end
			  else begin
			    // assign LEDS to LEDS_CONT
			    LEDS <= LEDS_CONT;
			  end
		  end
		  LOSS:  begin
		  // Move to IDLE State once the operated SWITCH is restored.
		    if(SWITCHES == 4'b0000)begin
			   next_state <= IDLE;
			 end
			 else begin
		  // If the operated switch is not restored, LEDs continue glowing in one hot fashion.
			   LEDS <= LEDS_CONT;
			 end
          end		  
		  WIN:   begin
		  // Make all LEDs glow Since the s/m is in WIN state.
		    LEDS <= 4'b1111;
		  end
		endcase

end



endmodule
