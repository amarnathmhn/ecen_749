`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:22:31 01/29/2016
// Design Name:   counter
// Module Name:   /homes/grad/amarnathmhn/ECEN_749/lab1/part2/counter/counter_tb.v
// Project Name:  counter
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: counter
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module counter_tb;

	// Inputs
	reg CLOCK;
	reg RESET;
	reg UP;
	reg DOWN;
	wire slowClock;
	wire [25:0]edgeCounter;
	
	

	// Outputs
	wire [3:0] LEDS;

	// Instantiate the Unit Under Test (UUT)
	counter uut (
		.CLOCK(CLOCK), 
		.RESET(RESET), 
		.UP(UP), 
		.DOWN(DOWN), 
		.LEDS(LEDS)
	);

assign slowClock = uut.slowClock;
assign edgeCounter = uut.edgeCounter;

initial begin
  CLOCK = 0;
  #50 CLOCK = 1;
end
	initial begin
		// Initialize Inputs
		//CLOCK = 0;
		
		RESET = 1;
		UP = 0;
		DOWN = 0;

		// Wait 100 ns for global reset to finish
		#100;
		CLOCK = 1;
		RESET = 0;
        
		// Add stimulus here
		
		UP = 1;
		#20 UP = 0;
		#200;
		UP = 1;
		#20 UP = 0;
		#200;
		UP = 1;
		#20 UP = 0;
		#200;
		DOWN = 1;
		#20 DOWN = 0;
		#200;
		DOWN = 1;
		#20 DOWN = 0;
		#10000;
		$finish;

	end
	
	always
	  #50 CLOCK = ~CLOCK;
      
endmodule

