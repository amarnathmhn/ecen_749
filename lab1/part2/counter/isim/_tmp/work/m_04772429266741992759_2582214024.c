/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                         */
/*  \   \        Copyright (c) 2003-2007 Xilinx, Inc.                 */
/*  /   /        All Right Reserved.                                  */
/* /---/   /\                                                         */
/* \   \  /  \                                                        */
/*  \___\/\___\                                                       */
/**********************************************************************/

/* This file is designed for use with ISim build 0xabfbedd0 */

#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/homes/grad/amarnathmhn/ECEN_749/lab1/part2/counter/counter.v";
static int ng1[] = {1, 0};
static unsigned int ng2[] = {2U, 0U};
static unsigned int ng3[] = {0U, 0U};
static int ng4[] = {0, 0};



static void C38_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    char *t20;

LAB0:    t1 = (t0 + 3512U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    t2 = (t0 + 2352);
    t3 = (t2 + 48U);
    t4 = *((char **)t3);
    t5 = (t0 + 4112);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t10 = (t9 + 4LL);
    t11 = 15U;
    t12 = t11;
    t13 = (t4 + 4LL);
    t14 = *((unsigned int *)t4);
    t11 = (t11 & t14);
    t15 = *((unsigned int *)t13);
    t12 = (t12 & t15);
    t16 = *((unsigned int *)t9);
    *((unsigned int *)t9) = (t16 & 4294967280U);
    t17 = *((unsigned int *)t9);
    *((unsigned int *)t9) = (t17 | t11);
    t18 = *((unsigned int *)t10);
    *((unsigned int *)t10) = (t18 & 4294967280U);
    t19 = *((unsigned int *)t10);
    *((unsigned int *)t10) = (t19 | t12);
    xsi_driver_vfirst_trans(t5, 0, 3);
    t20 = (t0 + 4016);
    *((int *)t20) = 1;

LAB1:    return;
}

static void A41_1(char *t0)
{
    char t4[8];
    char t23[8];
    char *t1;
    char *t2;
    char *t3;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t24;

LAB0:    t1 = (t0 + 3728U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(41, ng0);
    t2 = (t0 + 4032);
    *((int *)t2) = 1;
    t3 = (t0 + 3760);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(41, ng0);

LAB5:    xsi_set_current_line(43, ng0);
    t5 = (t0 + 1160U);
    t6 = *((char **)t5);
    memset(t4, 0, 8);
    t5 = (t4 + 4LL);
    t7 = (t6 + 4LL);
    t8 = *((unsigned int *)t7);
    t9 = (~(t8));
    t10 = *((unsigned int *)t6);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB9;

LAB7:    if (*((unsigned int *)t7) == 0)
        goto LAB6;

LAB8:    *((unsigned int *)t4) = 1;
    *((unsigned int *)t5) = 1;

LAB9:    t13 = (t4 + 4LL);
    t14 = *((unsigned int *)t13);
    t15 = (~(t14));
    t16 = *((unsigned int *)t4);
    t17 = (t16 & t15);
    t18 = (t17 != 0);
    if (t18 > 0)
        goto LAB10;

LAB11:    xsi_set_current_line(51, ng0);

LAB25:    xsi_set_current_line(52, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 2352);
    xsi_vlogvar_generic_wait_assign_value(t3, t2, 2, 0, 0, 4, 0LL);
    xsi_set_current_line(53, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 2512);
    xsi_vlogvar_generic_wait_assign_value(t3, t2, 1, 0, 0, 1, 0LL);
    xsi_set_current_line(54, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 2672);
    xsi_vlogvar_generic_wait_assign_value(t3, t2, 1, 0, 0, 1, 0LL);
    xsi_set_current_line(55, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 2032);
    xsi_vlogvar_generic_wait_assign_value(t3, t2, 1, 0, 0, 26, 0LL);
    xsi_set_current_line(56, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 2192);
    xsi_vlogvar_generic_wait_assign_value(t3, t2, 1, 0, 0, 1, 0LL);

LAB12:    goto LAB2;

LAB6:    *((unsigned int *)t4) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(43, ng0);

LAB13:    xsi_set_current_line(44, ng0);
    t19 = (t0 + 2032);
    t20 = (t19 + 48U);
    t21 = *((char **)t20);
    t22 = ((char*)((ng1)));
    memset(t23, 0, 8);
    xsi_vlog_unsigned_add(t23, 32, t21, 26, t22, 32);
    t24 = (t0 + 2032);
    xsi_vlogvar_generic_wait_assign_value(t24, t23, 2, 0, 0, 26, 0LL);
    xsi_set_current_line(45, ng0);
    t2 = (t0 + 2032);
    t3 = (t2 + 48U);
    t5 = *((char **)t3);
    t6 = ((char*)((ng2)));
    memset(t4, 0, 8);
    t7 = (t4 + 4LL);
    t13 = (t5 + 4LL);
    t19 = (t6 + 4LL);
    if (*((unsigned int *)t13) != 0)
        goto LAB15;

LAB14:    if (*((unsigned int *)t19) != 0)
        goto LAB15;

LAB18:    if (*((unsigned int *)t5) < *((unsigned int *)t6))
        goto LAB17;

LAB16:    *((unsigned int *)t4) = 1;

LAB17:    t20 = (t4 + 4LL);
    t8 = *((unsigned int *)t20);
    t9 = (~(t8));
    t10 = *((unsigned int *)t4);
    t11 = (t10 & t9);
    t12 = (t11 != 0);
    if (t12 > 0)
        goto LAB19;

LAB20:
LAB21:    goto LAB12;

LAB15:    *((unsigned int *)t4) = 1;
    *((unsigned int *)t7) = 1;
    goto LAB17;

LAB19:    xsi_set_current_line(45, ng0);

LAB22:    xsi_set_current_line(46, ng0);
    t21 = ((char*)((ng3)));
    t22 = (t0 + 2032);
    xsi_vlogvar_generic_wait_assign_value(t22, t21, 2, 0, 0, 26, 0LL);
    xsi_set_current_line(47, ng0);
    t2 = (t0 + 2192);
    t3 = (t2 + 48U);
    t5 = *((char **)t3);
    memset(t4, 0, 8);
    t6 = (t4 + 4LL);
    t7 = (t5 + 4LL);
    t8 = *((unsigned int *)t5);
    t9 = (~(t8));
    *((unsigned int *)t4) = t9;
    *((unsigned int *)t6) = 0;
    if (*((unsigned int *)t7) != 0)
        goto LAB24;

LAB23:    t15 = *((unsigned int *)t4);
    *((unsigned int *)t4) = (t15 & 1U);
    t16 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t16 & 1U);
    t13 = (t0 + 2192);
    xsi_vlogvar_generic_wait_assign_value(t13, t4, 2, 0, 0, 1, 0LL);
    xsi_set_current_line(48, ng0);
    t2 = (t0 + 2352);
    t3 = (t2 + 48U);
    t5 = *((char **)t3);
    t6 = ((char*)((ng1)));
    memset(t4, 0, 8);
    xsi_vlog_unsigned_add(t4, 32, t5, 4, t6, 32);
    t7 = (t0 + 2352);
    xsi_vlogvar_generic_wait_assign_value(t7, t4, 2, 0, 0, 4, 0LL);
    goto LAB21;

LAB24:    t10 = *((unsigned int *)t4);
    t11 = *((unsigned int *)t7);
    *((unsigned int *)t4) = (t10 | t11);
    t12 = *((unsigned int *)t6);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t6) = (t12 | t14);
    goto LAB23;

}


extern void work_m_04772429266741992759_2582214024_init()
{
	static char *pe[] = {(void *)C38_0,(void *)A41_1};
	xsi_register_didat("work_m_04772429266741992759_2582214024", "isim/_tmp/work/m_04772429266741992759_2582214024.didat");
	xsi_register_executes(pe);
}
