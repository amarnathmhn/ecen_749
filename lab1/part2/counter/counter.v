`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:52:15 01/29/2016 
// Design Name: 
// Module Name:    counter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

// Module implementing the 4-bit counter
module counter(
    input CLOCK,
    input RESET,
    input UP,
    input DOWN,
    output reg [3:0] LEDS
    );

reg [25:0] edgeCounter = 26'd0;    // Register Holding the Number of Rising CLOCK Edges Counted 
reg slowClock;              // slowClock that is used so that LED Blinking is visible. 

// Block that Counts the Rising CLOCK Edges of the 100MHz clock and Toggles the
// slowClock so that its period is 1 second with 50% duty cycle
always @(posedge CLOCK) begin

	 edgeCounter <= edgeCounter + 1;
    if(edgeCounter >= 26'd50000000) begin
	   
		 edgeCounter <= 26'd0;		 
		 slowClock <= ~slowClock;	 		 
		
	 end

	
end

   
// Block that implements the counter based on slowClock.
always @(posedge slowClock or posedge RESET)  begin
    
	// Startover the Count on High Value of RESET 
	 if(RESET) begin		 	 		 
		LEDS        <= 4'b0000;
	 end
	 // Increment counter value on High  value of UP 
	 else if (UP) begin
	    
	    LEDS <= LEDS + 1'b1;
		 
	 end
	 // Decrement counter value on HIGH  value of DOWN
	 else if (DOWN) begin
	    
	    LEDS <= LEDS - 1'b1; 
		 
	 end

end	

endmodule
