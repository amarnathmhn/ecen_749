#include<xparameters.h>
//#include<xgpio.h>
#include<multiply.h>
#define WAIT_VAL 0x1000000
int delay(void);
int main() {
   
	// result for operand1*operand2
   u32 result     = 0;
   int i = 0;
	// As an example to demonstrate multiplication
	// For 0 to 16, multiply the number with itself to produce a sequence of square numbers
   for(i=0;i<17;i++){ 
     // Write to register holding operand1
	  MULTIPLY_mWriteSlaveReg0(XPAR_MULTIPLY_0_BASEADDR,0,i);
	  // Write to register holding operand2
     MULTIPLY_mWriteSlaveReg1(XPAR_MULTIPLY_0_BASEADDR,0,i);
	  // Delay before getting and printing the result
     delay();
     result = MULTIPLY_mReadReg(XPAR_MULTIPLY_0_BASEADDR,MULTIPLY_SLV_REG2_OFFSET);
     xil_printf("Op1: %d Op2: %d Result:%d\n\r",i, i, result);
  } 
  
   
}

// Define a delay function that runs a count WAIT_VAL times so that the LEDs
// can be updated at approximately every 1 second
int delay(void){
// Declare delay_count as volatile to avoid code optimization by the compiler
  volatile int delay_count = 0;
  while(delay_count < WAIT_VAL)
     delay_count++;
  return(0);
}



