#include<xparameters.h>
#include<xgpio.h>
#define WAIT_VAL 0x1000000

int delay (void);

int main(){
  int count;           /* holds the count value*/
  int count_masked;    /* holds the least 4 bits of count*/
  XGpio led;           /* Gpio instance for LEDS */
  XGpio push_dip;      /* Gpio instance for PUSH_DIP */
  u32   push_dip_val;  /* Equivalent value of 9 bit push_dip gpio*/
  u32   push_val;      /* Holds the 5bit Push button value PUSH_DIP[8:4]*/
  u32   dip_val;       /* Holds the DIP switch state*/
  u32   leds_val;      /* Holds the LED bit equivalent value*/
   
  XGpio_Initialize(&led,XPAR_LEDS_DEVICE_ID); /*Initialize led*/
  XGpio_Initialize(&push_dip,XPAR_PUSH_DIP_DEVICE_ID);  /*Initialize push_dip*/
  XGpio_SetDataDirection(&led,1,0); /*Set led data direction*/
  XGpio_SetDataDirection(&push_dip,1,0xffffffff); /*set push_dip gpio data direction*/
  count = 0; /*Initialize count value*/
  while(1) {
   
	 push_dip_val = XGpio_DiscreteRead(&push_dip,1);
	 // Get the corresponding PUSH button equivalent value
	 push_val     = (push_dip_val >> 4) & 0x1f;
    count_masked = count & 0xf;	 
	 
	 switch(push_val){
		
		// Push Button U8 North is pressed
		case 1: count++;
		        count_masked = count & 0xf;
				  xil_printf("North Push Button Pressed => Increment count : count = %d\n\r",count_masked);             		
		        break;
		    
		// Push Button V8 south is pressed
		case 2: count--;
		        count_masked = count & 0xf;
				  xil_printf("South Push Button Pressed => Decrement count : count = %d\n\r",count_masked); 
		        break;
		
		// Push Button AK7 East is pressed 
		// Status of DIP switches is displayed
		case 4 : dip_val = push_dip_val & 0xf;
		         xil_printf("East Push Button Pressed = > Display DIP Switch Status :\n\r"); 
		         XGpio_DiscreteWrite(&led,1,dip_val);
	 	         break;
		
		// Push Button AJ7 West is pressed. 
		// Display Current Count Value on the LEDS
		case 8: xil_printf("West Push Button Pressed => Display Current Count Value on LEDS :\n\r"); 
	           XGpio_DiscreteWrite(&led,1,count_masked);		
				  break;
		
		// Push Button AJ6 Center is pressed
		case 16: count = 0;
		         count_masked = count & 0xf;
					xil_printf("Center Push Button Pressed => Reset the Count : count = %d\n\r",count_masked);  
		         break;
		// if no push button is pressed, LEDs assigned to 0.
		default :XGpio_DiscreteWrite(&led,1,0);
		         break;
	 }
	 // Always read and print LED value whenever push button is pressed;
	 if(push_val) {
 	  leds_val = XGpio_DiscreteRead(&led,1);
	  xil_printf("LEDS = 0x%x\n\r",leds_val); 
    }
	 
	 delay();
    
  }

  return (0);
}
// Define a delay function that runs a count WAIT_VAL times so that the LEDs
// can be updated at approximately every 1 second
int delay(void){
// Declare delay_count as volatile to avoid code optimization by the compiler
  volatile int delay_count = 0;
  while(delay_count < WAIT_VAL)
     delay_count++;
  return(0);
}


