#include<xparameters.h>
#include<xgpio.h>
#define WAIT_VAL 0x1000000

// Declare delay function responsible for delaying loop iteration.
int delay (void);

int main(){
  int count; // Holds the count value
  int count_masked; // Holds the lower 4 bits of count
  XGpio led; // gpio instance corresponding to LED gpio
  // Initialize led instance to LED device
  XGpio_Initialize(&led,XPAR_LEDS_DEVICE_ID);
  
  // Set the Data direction for LED as output(0);
  XGpio_SetDataDirection(&led,1,0);
  
  // Initialize count;
  count = 0;
  while(1) {
  // Take lower 4 bits of count value to display on LEDs
    count_masked = count & 0xf;
  // Write masked count to the LED gpio
    XGpio_DiscreteWrite(&led, 1, count_masked);
	 
  // Print the masked count value to the serial console
    xil_printf("LEDS = 0x%x\n\r",count_masked);
	 
  // wait for WAIT_VAL cycles of 125 MHz clock
    delay();
	 
  // Increment count value
    count++;
    
  }

  return (0);
}

// Implements the delay function
int delay(void){
  volatile int delay_count = 0;
  // Count until the delay equals WAIT_VAL cycles of 125MHz clock
  while(delay_count < WAIT_VAL)
     delay_count++;
  return(0);
}

